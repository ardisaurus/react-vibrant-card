import React, { Component } from "react";
import Cardo from "./components/Cardo";

export class App extends Component {
  render() {
    return (
      <div>
        <Cardo />
      </div>
    );
  }
}

export default App;
