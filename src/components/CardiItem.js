import React, { Component } from "react";
import Vibrant from "node-vibrant";

export class Cardo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      colorPallette: {
        Vibrant: {},
        Muted: {},
        LightVibrant: {},
        LightMuted: {},
        DarkVibrant: {},
        DarkMuted: {}
      }
    };
    this.getPallete(this.props.song);
  }

  async getPallete(song) {
    await Vibrant.from(song.pic)
      .getPalette()
      .then(palette => {
        this.setState({
          colorPallette: { ...palette }
        });
      });
  }

  getCardStyle = () => {
    return {
      margin: "0 auto",
      background: `#fff`,
      textAlign: "center",
      transition: "0.3s",
      width: "16em",
      borderRadius: "18px"
    };
  };

  getCardHeadStyle = pic => {
    return {
      margin: "0.4em",
      width: "95%",
      height: "16em",
      position: "relative",
      background: 'url("' + pic + '") no-repeat center center/cover',
      borderRadius: "18px"
    };
  };

  getColorCircleStyle = color => {
    return {
      margin: "0 auto",
      background: color,
      width: "50px",
      height: "50px",
      borderRadius: "50%"
    };
  };

  render() {
    const song = this.props.song;
    return (
      <div key={song.id} style={this.getCardStyle()}>
        <div style={this.getCardHeadStyle(song.pic)}></div>
        <div className="card-content">
          <div
            style={this.getColorCircleStyle(
              this.state.colorPallette.Vibrant.hex
            )}
          ></div>
          vibrant : {this.state.colorPallette.Vibrant.hex}
          <div
            style={this.getColorCircleStyle(this.state.colorPallette.Muted.hex)}
          ></div>
          muted : {this.state.colorPallette.Muted.hex}
          <div
            style={this.getColorCircleStyle(
              this.state.colorPallette.LightVibrant.hex
            )}
          ></div>
          light vibrant : {this.state.colorPallette.LightVibrant.hex}
          <div
            style={this.getColorCircleStyle(
              this.state.colorPallette.LightMuted.hex
            )}
          ></div>
          light muted : {this.state.colorPallette.LightMuted.hex}
          <div
            style={this.getColorCircleStyle(
              this.state.colorPallette.DarkVibrant.hex
            )}
          ></div>
          dark vibrant : {this.state.colorPallette.DarkVibrant.hex}
          <div
            style={this.getColorCircleStyle(
              this.state.colorPallette.DarkMuted.hex
            )}
          ></div>
          dark muted : {this.state.colorPallette.DarkMuted.hex}
        </div>
      </div>
    );
  }
}
export default Cardo;
