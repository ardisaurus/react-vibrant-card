import React, { Component } from "react";
import Vibrant from "node-vibrant";

export class Cardo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      colorPallette: {
        Vibrant: {},
        Muted: {},
        LightVibrant: {},
        LightMuted: {},
        DarkVibrant: {},
        DarkMuted: {}
      }
    };
    this.getPallete(this.props.song);
  }

  async getPallete(song) {
    await Vibrant.from(song.pic)
      .getPalette()
      .then(palette => {
        this.setState({
          colorPallette: { ...palette }
        });
      });
  }

  getCardStyle = () => {
    return {
      margin: "0 auto",
      boxShadow:
        "0 4px 8px 0 rgba(0, 0, 0, 0.2), inset 0 -20px 2em -15px #292929",
      background: `${this.state.colorPallette.DarkVibrant.hex}`,
      textAlign: "center",
      transition: "0.3s",
      width: "16em",
      borderRadius: "18px"
    };
  };

  getCardHeadStyle = pic => {
    return {
      width: "100%",
      height: "16em",
      position: "relative",
      background: 'url("' + pic + '") no-repeat center center/cover',
      borderRadius: "18px 18px 0 0"
    };
  };

  getCardHeadShadowStyle = () => {
    return {
      content: "",
      position: "absolute",
      bottom: 0,
      left: 0,
      width: "100%",
      height: "25%",
      zIndex: 1,
      background: `rgba(${this.state.colorPallette.Vibrant.r}, ${this.state.colorPallette.Vibrant.g}, ${this.state.colorPallette.Vibrant.b}, 0.2)`,
      boxShadow: `inset 0 -40px 50px 10px ${this.state.colorPallette.Vibrant.hex}`,
      borderRadius: "100% 0 0 0"
    };
  };

  getCardHeadShadowStyle2 = () => {
    return {
      content: "",
      position: "absolute",
      bottom: 0,
      left: 0,
      width: "100%",
      height: "30%",
      zIndex: 2,
      background: `rgba(${this.state.colorPallette.DarkVibrant.r}, ${this.state.colorPallette.DarkVibrant.g}, ${this.state.colorPallette.DarkVibrant.b}, 0.2)`,
      boxShadow: `inset 0 -40px 50px 10px ${this.state.colorPallette.DarkVibrant.hex}`,
      borderRadius: "0 100% 0 0"
    };
  };

  getHeadTitleStyle = () => {
    return {
      position: "absolute",
      bottom: 0,
      left: 0,
      right: 0,
      zIndex: 3
    };
  };

  getSongTitleStyle = () => {
    return {
      color: `${this.state.colorPallette.LightVibrant.hex}`,
      fontSize: "150%"
    };
  };

  render() {
    const song = this.props.song;
    return (
      <div key={song.id} style={this.getCardStyle()}>
        <div style={this.getCardHeadStyle(song.pic)}>
          <div style={this.getCardHeadShadowStyle()}></div>
          <div style={this.getCardHeadShadowStyle2()}></div>
          <div className="card-header-title" style={this.getHeadTitleStyle()}>
            <p>
              <b style={this.getSongTitleStyle()}>
                {song.title.length < 16
                  ? song.title
                  : song.title.substring(0, 15) + "..."}
              </b>
            </p>
            <p>
              <small
                style={{
                  color: `${this.state.colorPallette.LightMuted.hex}`
                }}
              >
                {song.artist}
              </small>
            </p>
          </div>
        </div>
        <div
          className="card-content"
          style={{
            color: `${this.state.colorPallette.LightMuted.hex}`
          }}
        >
          <div className="card-content-bottom">
            <div className="card-content-bottom-left">
              <p>
                <b>{song.duration.min}</b>
                <small>min</small>
              </p>
              <p>
                <b>{song.duration.sec}</b>
                <small>sec</small>
              </p>
            </div>
            <div className="card-content-bottom-right">
              <p>
                #<b>{song.track}</b>
              </p>
              <p>{song.album}</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Cardo;
