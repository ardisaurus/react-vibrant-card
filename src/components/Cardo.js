import React, { Component } from "react";
import CardiItem from "./CardiItem";
import CardoItem from "./CardoItem";
import CardaItem from "./CardaItem";
import CarduItem from "./CarduItem";

export class Cardo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      playlist: [
        {
          id: 0,
          title: "Without me",
          artist: "Halsey",
          duration: {
            min: "03",
            sec: "48"
          },
          track: "1",
          album: "Without me",
          pic: require("../upload/halsey.png"),
          ava: require("../upload/halsey-avatar.jpeg")
        },
        {
          id: 1,
          title: "So far away",
          artist: "Martin Garrix",
          duration: {
            min: "03",
            sec: "48"
          },
          track: "1",
          album: "Single",
          pic: require("../upload/mtgx.jpg"),
          ava: require("../upload/mtgx-avatar.jpeg")
        },
        {
          id: 2,
          title: "Colder",
          artist: "Nina Nesbit",
          duration: {
            min: "03",
            sec: "48"
          },
          track: "1",
          album: "Colder",
          pic: require("../upload/nina.jpg"),
          ava: require("../upload/nina-avatar.jpeg")
        },
        {
          id: 3,
          title: "Miss Myself",
          artist: "NOTD feat. HRVY",
          duration: {
            min: "03",
            sec: "48"
          },
          track: "1",
          album: "Single",
          pic: require("../upload/notd.jpg"),
          ava: require("../upload/notd-avatar.jpeg")
        },
        {
          id: 4,
          title: "Who do you love",
          artist: "The Chainsmokers feat. 5SOS",
          duration: {
            min: "03",
            sec: "48"
          },
          track: "1",
          album: "Single",
          pic: require("../upload/tcs.png"),
          ava: require("../upload/tcs-avatar.png")
        }
      ]
    };
  }
  render() {
    return (
      <div className="cardList">
        {this.state.playlist.map(song => {
          return <CardiItem key={song.id} song={song} />;
        })}
        {this.state.playlist.map(song => {
          return <CardoItem key={song.id} song={song} />;
        })}
        {this.state.playlist.map(song => {
          return <CardaItem key={song.id} song={song} />;
        })}
        {this.state.playlist.map(song => {
          return <CarduItem key={song.id} song={song} />;
        })}
      </div>
    );
  }
}
export default Cardo;
